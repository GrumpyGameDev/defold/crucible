local BEDROOM = "bedroom"

local data = {}

data[BEDROOM]={}
data[BEDROOM].title = "Your Bedroom"
data[BEDROOM].description = { "This is where you", "sleep." }

local module = {}

module.BEDROOM = function() return BEDROOM end

module.get = function(location)
	return data[location]
end

return module