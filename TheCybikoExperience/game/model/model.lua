local avatar = require("game.model.avatar")
local locations = require("game.model.locations")
local actions = require("game.model.actions")

local module = {}

module.init = function ()
end

module.reset = function()
	avatar.reset()
end

module.avatar = function()
	return avatar
end

module.locations = function()
	return locations
end

module.actions = function()
	return actions
end

module.reset()

return module