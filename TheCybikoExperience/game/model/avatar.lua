local locations = require("game.model.locations")
local actions = require("game.model.actions")
local constants = require("game.model.constants")
local data = {}

local module = {}

module.reset = function()
	data.money = constants.INITIAL_MONEY()
	data.health = constants.INITIAL_HEALTH()
	data.maximumHealth = constants.INITIAL_HEALTH()
	data.hunger = constants.INITIAL_HUNGER()
	data.fatigue = constants.INITIAL_FATIGUE()
	data.day = constants.INITIAL_DAY()
	data.hour = constants.INITIAL_HOUR()
	data.minute = constants.INITIAL_MINUTE()
	data.location = locations.BEDROOM()
end

module.data = function()
	return data
end

module.mayAct = function(action)
	if action == actions.STATUS() then
		return true
	else
		return false
	end
end

module.location = function()
	return data.location
end

return module