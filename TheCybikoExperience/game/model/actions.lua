local EAT = "eat"
local SLEEP = "sleep"
local STATUS = "status"

local all = {STATUS, EAT, SLEEP}

local module = {}

module.EAT = function() return EAT end
module.SLEEP = function() return SLEEP end
module.STATUS = function() return STATUS end
module.all = function() return all end

return module