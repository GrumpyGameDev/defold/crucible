local locations = require("game.model.locations")

local INITIAL_HEALTH = 100
local INITIAL_MONEY = 100
local INITIAL_HUNGER = 0
local INITIAL_FATIGUE = 0
local INITIAL_DAY = 1
local INITIAL_HOUR = 6
local INITIAL_MINUTE = 0
local INITIAL_LOCATION = locations.BEDROOM()

local module = {}

module.INITIAL_HEALTH = function() return INITIAL_HEALTH end
module.INITIAL_MONEY = function() return INITIAL_MONEY end
module.INITIAL_HUNGER = function() return INITIAL_HUNGER end
module.INITIAL_FATIGUE = function() return INITIAL_FATIGUE end
module.INITIAL_DAY = function() return INITIAL_DAY end
module.INITIAL_HOUR = function() return INITIAL_HOUR end
module.INITIAL_MINUTE = function() return INITIAL_MINUTE end
module.INITIAL_LOCATION = function() return INITIAL_LOCATION end

return module