local module = {}

module.fif = function(condition,whenTrue,whenFalse)
	if condition then
		return whenTrue
	else
		return whenFalse
	end
end

return module