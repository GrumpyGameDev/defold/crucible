local SPLASH = "splash"
local MAIN_MENU = "mainMenu"
local ABOUT = "about"
local INSTRUCTIONS = "instructions"
local OPTIONS = "options"
local AT_LOCATION = "atLocation"

local module = {}

module.SPLASH = function() return SPLASH end
module.MAIN_MENU = function() return MAIN_MENU end
module.ABOUT = function() return ABOUT end
module.INSTRUCTIONS = function() return INSTRUCTIONS end
module.OPTIONS = function() return OPTIONS end
module.AT_LOCATION = function() return AT_LOCATION end

return module