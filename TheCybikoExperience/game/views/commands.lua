local module = {}

module.LEFT = function ()
	return "left"
end

module.RIGHT = function ()
	return "right"
end

module.UP = function ()
	return "up"
end

module.DOWN = function ()
	return "down"
end

module.GREEN = function ()
	return "green"
end

module.RED = function ()
	return "red"
end



return module