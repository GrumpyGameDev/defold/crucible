local data = {}
local CELL_COLUMNS = 20
local CELL_ROWS = 12
local BACKGROUND_LAYER = "background"
local FOREGROUND_LAYER = "foreground"
local COLOR_BLACK = 0
local COLOR_DKGRAY = 1
local COLOR_LTGRAY = 2
local COLOR_WHITE = 3

local module = {}

module.init = function(url)
	data.url = url
end

module.CELL_COLUMNS = function()
	return CELL_COLUMNS
end

module.CELL_ROWS = function()
	return CELL_ROWS
end

module.COLOR_BLACK = function()
	return COLOR_BLACK
end

module.COLOR_DKGRAY = function()
	return COLOR_DKGRAY
end

module.COLOR_LTGRAY = function()
	return COLOR_LTGRAY
end

module.COLOR_WHITE = function()
	return COLOR_WHITE
end

module.putBackground = function(x,y,color) 
	tilemap.set_tile(data.url, BACKGROUND_LAYER , x, y, 0xDB + 256 * color + 1)
end

module.putForeground = function(x,y,color,character)
	tilemap.set_tile(data.url, FOREGROUND_LAYER , x, y, character + 256 * color + 1)
end

module.putTile = function(x,y,background,foreground,character)
	module.putBackground(x,y,background)
	module.putForeground(x,y,foreground,character)
end

module.fillRect = function(x1,y1,x2,y2,background,foreground,character)
	for x = x1,x2 do
		for y = y1,y2 do
			module.putTile(x,y,background,foreground,character)
		end
	end
end

module.clear = function(background,foreground,character)
	module.fillRect(1,1,CELL_COLUMNS,CELL_ROWS,background,foreground,character)
end

module.writeText = function(x,y,background,foreground,text)
	for index = 1, string.len(text) do
		module.putTile(x,y,background,foreground,string.byte(text,index))
		x = x + 1
	end
end

return module