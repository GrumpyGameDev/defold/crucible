local current = require("game.views.current")
local screen = require("game.views.screen")
local commands = require("game.views.commands")
local viewStates = require("game.views.viewStates")

local module = {}

module.init = function ()
end

module.update = function ()
	screen.clear(screen.COLOR_WHITE(),screen.COLOR_BLACK(),0)
	screen.writeText(1,screen.CELL_ROWS(),screen.COLOR_WHITE(),screen.COLOR_BLACK(),"The")
	screen.writeText(1,screen.CELL_ROWS()-1,screen.COLOR_WHITE(),screen.COLOR_BLACK(),"Cybiko")
	screen.writeText(1,screen.CELL_ROWS()-2,screen.COLOR_WHITE(),screen.COLOR_BLACK(),"Experience")
	screen.writeText(1,screen.CELL_ROWS()-6,screen.COLOR_WHITE(),screen.COLOR_LTGRAY(),"A \"game\" by")
	screen.writeText(1,screen.CELL_ROWS()-7,screen.COLOR_WHITE(),screen.COLOR_LTGRAY(),"TheGrumpyGameDev")
	screen.writeText(1,1,screen.COLOR_WHITE(),screen.COLOR_DKGRAY(),"<Z> to start")
end

module.handleCommand = function(command)
	if command == commands.GREEN() then
		current.set(viewStates.MAIN_MENU())
	end
end

return module