local current = require("game.views.current")
local screen = require("game.views.screen")
local commands = require("game.views.commands")
local viewStates = require("game.views.viewStates")

local module = {}

module.init = function ()
end

module.update = function ()
	screen.clear(screen.COLOR_WHITE(),screen.COLOR_BLACK(),0)
	screen.writeText(1,screen.CELL_ROWS(),screen.COLOR_BLACK(),screen.COLOR_WHITE(),"About               ")
	screen.writeText(1,screen.CELL_ROWS()-1,screen.COLOR_WHITE(),screen.COLOR_DKGRAY(),"TODO")
end

module.handleCommand = function(command)
	if command == commands.GREEN() or command == commands.RED() then
		current.set(viewStates.MAIN_MENU())
	end
end

return module