local current = require("game.views.current")
local screen = require("game.views.screen")
local commands = require("game.views.commands")
local viewStates = require("game.views.viewStates")

local module = {}

module.init = function ()
end

module.update = function ()
	screen.clear(screen.COLOR_WHITE(),screen.COLOR_BLACK(),0)
	screen.writeText(1,screen.CELL_ROWS(), screen.COLOR_BLACK(), screen.COLOR_WHITE(),"Instructions        ")
	screen.writeText(1,screen.CELL_ROWS()-1, screen.COLOR_WHITE(), screen.COLOR_DKGRAY(),"Controls:")
	screen.writeText(1,screen.CELL_ROWS()-3, screen.COLOR_WHITE(), screen.COLOR_DKGRAY(),"Arrow Keys:")
	screen.writeText(1,screen.CELL_ROWS()-4, screen.COLOR_WHITE(), screen.COLOR_DKGRAY(),"  Move/Select")
	screen.writeText(1,screen.CELL_ROWS()-6, screen.COLOR_WHITE(), screen.COLOR_DKGRAY(),"Z: Confirm")
	screen.writeText(1,screen.CELL_ROWS()-8, screen.COLOR_WHITE(), screen.COLOR_DKGRAY(),"X: Cancel")
end

module.handleCommand = function(command)
	if command == commands.GREEN() or command == commands.RED() then
		current.set(viewStates.MAIN_MENU())
	end
end

return module