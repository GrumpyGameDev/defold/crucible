local current = require("game.views.current")
local screen = require("game.views.screen")
local commands = require("game.views.commands")
local viewStates = require("game.views.viewStates")
local model = require("game.model.model")
local engine = require("game.engine.engine")

local data = {}
data.actionText = {}
data.actionText[model.actions().EAT()]    = "       Eat...       "
data.actionText[model.actions().SLEEP()]  = "      Sleep...      "
data.actionText[model.actions().STATUS()] = "    Check Status    "

data.menuItems = {}
data.current = 1

local module = {}

module.init = function ()
end

local updateMenuItems = function()
	data.menuItems = engine.doCommand(engine.commands().createQueryActions()).payload
	if data.current > #data.menuItems then
		data.current = 1
	end
end

module.update = function ()
	screen.clear(screen.COLOR_WHITE(),screen.COLOR_BLACK(),0)
	local location = model.locations().get(model.avatar().location())
	local line = screen.CELL_ROWS()
	screen.writeText(1, line, screen.COLOR_WHITE(), screen.COLOR_BLACK(), location.title)
	line = line - 1
	for i,v in ipairs(location.description) do
		screen.writeText(1, line, screen.COLOR_WHITE(), screen.COLOR_DKGRAY(), v)
		line = line - 1
	end
	updateMenuItems()
	screen.writeText(1,2,screen.COLOR_LTGRAY(),screen.COLOR_DKGRAY(),"     <Z> Action     ")
	if data.current>1 then
		screen.putTile(1,2,screen.COLOR_LTGRAY(),screen.COLOR_DKGRAY(),27)
	end	
	if data.current<#data.menuItems then
		screen.putTile(screen.CELL_COLUMNS(),2,screen.COLOR_LTGRAY(),screen.COLOR_DKGRAY(),26)
	end
	screen.writeText(1,1,screen.COLOR_LTGRAY(),screen.COLOR_BLACK(),data.actionText[data.menuItems[data.current]])
end

local handleLeftCommand = function()
	if data.current>1 then
		data.current = data.current - 1
	end
end

local handleRightCommand = function()
	if data.current<#data.menuItems then
		data.current = data.current + 1
	end
end

local handleGreenCommand = function()
end

local commandHandlers = {}
commandHandlers[commands.LEFT()]=handleLeftCommand
commandHandlers[commands.RIGHT()]=handleRightCommand
commandHandlers[commands.GREEN()]=handleGreenCommand

module.handleCommand = function(command)
	local commandHandler = commandHandlers[command]
	if commandHandler~=nil then
		commandHandler()
	end
end

return module