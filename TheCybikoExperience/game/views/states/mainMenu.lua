local current = require("game.views.current")
local screen = require("game.views.screen")
local commands = require("game.views.commands")
local viewStates = require("game.views.viewStates")
local utility = require("game.misc.utility")
local engine = require("game.engine.engine")

local MENU_ITEM_START        = "Start               "
local MENU_ITEM_INSTRUCTIONS = "Instructions        "
local MENU_ITEM_OPTIONS      = "Options             "
local MENU_ITEM_ABOUT        = "About               "

local data = {}
data.current = 1
data.items = {}
table.insert(data.items, MENU_ITEM_START)
table.insert(data.items, MENU_ITEM_INSTRUCTIONS)
table.insert(data.items, MENU_ITEM_OPTIONS)
table.insert(data.items, MENU_ITEM_ABOUT)

local nextMenuItem = function()
	data.current = data.current + 1
	while data.current > #data.items do
		data.current = data.current - #data.items
	end
end

local previousMenuItem = function()
	data.current = data.current - 1
	while data.current < 1 do
		data.current = data.current + #data.items
	end
end

local executeMenuItem = function()
	local menuItem = data.items[data.current]
	if menuItem == MENU_ITEM_START then
		engine.doCommand(engine.commands().createReset())
		current.set(viewStates.AT_LOCATION())	
	elseif menuItem == MENU_ITEM_INSTRUCTIONS then
		current.set(viewStates.INSTRUCTIONS())	
	elseif menuItem == MENU_ITEM_OPTIONS then
		current.set(viewStates.OPTIONS())	
	elseif menuItem == MENU_ITEM_ABOUT then
		current.set(viewStates.ABOUT())	
	end
end

local module = {}

module.init = function ()
end

module.update = function ()
	screen.clear(screen.COLOR_WHITE(),screen.COLOR_BLACK(),0)
	screen.writeText(1,screen.CELL_ROWS(),screen.COLOR_BLACK(),screen.COLOR_WHITE(),"Main Menu           ")
	for i, v in ipairs(data.items) do
		local bg = utility.fif(i==data.current, screen.COLOR_DKGRAY(), screen.COLOR_WHITE())
		local fg = utility.fif(i==data.current, screen.COLOR_WHITE(), screen.COLOR_DKGRAY())
		screen.writeText(1, screen.CELL_ROWS()-i, bg, fg, v)
	end
end

local table = {}
table[commands.UP()]=previousMenuItem
table[commands.DOWN()]=nextMenuItem
table[commands.GREEN()]=executeMenuItem

module.handleCommand = function(command)
	local handler = table[command]
	if handler~=nil then
		handler()
	end
end

return module