local data = {}

local module = {}

module.set = function(value)
	data.current = value
end

module.get = function()
	return data.current
end

return module