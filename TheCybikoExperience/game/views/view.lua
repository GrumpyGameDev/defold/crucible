local model = require("game.model.model")
local screen = require("game.views.screen")
local viewStates = require("game.views.viewStates")
local current = require("game.views.current")
local commands = require("game.views.commands")
local engine = require("game.engine.engine")

local data = {}
data.views = {}
data.views[viewStates.SPLASH()] = require("game.views.states.splash")
data.views[viewStates.MAIN_MENU()] = require("game.views.states.mainMenu")
data.views[viewStates.ABOUT()] = require("game.views.states.about")
data.views[viewStates.INSTRUCTIONS()] = require("game.views.states.instructions")
data.views[viewStates.OPTIONS()] = require("game.views.states.options")
data.views[viewStates.AT_LOCATION()] = require("game.views.states.atLocation")

local module = {}

module.init = function (url)
	screen.init(url)
	engine.init()
	current.set(viewStates.SPLASH())
	for k,v in pairs(data.views) do
		v.init()
	end
end

module.update = function ()
	data.views[current.get()].update()
end

module.handleCommand = function(command)
	data.views[current.get()].handleCommand(command)
end

module.commands = function ()
	return commands
end

return module