local RESET = "reset"
local QUERY_ACTIONS = "queryActions"

local createReset = function()
	local command = {}
	command.type = RESET
	return command
end

local createQueryActions = function()
	local command = {}
	command.type = QUERY_ACTIONS
	return command
end

local module = {}

module.RESET = function() return RESET end
module.QUERY_ACTIONS = function() return QUERY_ACTIONS end

module.createReset = createReset
module.createQueryActions = createQueryActions

return module