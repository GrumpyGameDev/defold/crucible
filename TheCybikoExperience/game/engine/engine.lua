local model = require("game.model.model")
local commands = require("game.engine.commands")
local world = require("game.engine.world")
local result = require("game.engine.result")

local onReset = function(command)
	world.reset()
	return result.create(true)
end

local onQueryActions = function(command)
	return result.create(true,world.queryActions())
end

local data = {}
data.handlers = {}
data.handlers[commands.RESET()] = onReset
data.handlers[commands.QUERY_ACTIONS()] = onQueryActions

local module = {}

module.commands = function()
	return commands
end

module.init = function ()
end

module.doCommand = function(command)
	if command==nil or command.type==nil then
		return result.create(false,{"Invalid command!"})
	else
		local handler = data.handlers[command.type]
		if handler~=nil then
			return handler(command)
		else
			return result.create(false,{"Did not find handler for command type"..command.type.."."})
		end
	end
end

return module