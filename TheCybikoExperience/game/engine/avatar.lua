local model = require("game.model.model")

local module = {}

module.reset = function()
end

module.mayAct = function(action)
	return model.avatar().mayAct(action)
end

return module