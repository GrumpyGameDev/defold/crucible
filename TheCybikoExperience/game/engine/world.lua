local avatar = require("game.engine.avatar")
local model = require("game.model.model")

local module = {}

module.avatar = function() return avatar end

module.reset = function()
	model.reset()
end

module.queryActions = function()
	local allActions = model.actions().all()
	local availableActions = {}
	for i,v in ipairs(allActions) do
		print(avatar.mayAct)
		if avatar.mayAct(v) then
			table.insert(availableActions,v)
		end
	end
	return availableActions
end

return module