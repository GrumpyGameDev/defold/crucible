local module = {}

module.create = function (result,payload)
	return {result = result or false, payload = payload or {}}
end

return module