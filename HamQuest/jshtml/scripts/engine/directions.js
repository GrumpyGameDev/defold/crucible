var Directions ={
    "NORTH": function() { return "north"; },
    "EAST": function() { return "east"; },
    "SOUTH": function() { return "south"; },
    "WEST": function() { return "west"; },
    "getNextX": function(direction, x, y) {
        if(direction===this.EAST()){
            return x + 1;
        }else if(direction===this.WEST()){
            return x - 1;
        }else{
            return x;
        }
    },
    "getNextY": function(direction, x, y) {
        if(direction===this.NORTH()){
            return y - 1;
        }else if(direction===this.SOUTH()){
            return y + 1;
        }else{
            return y;
        }
    }
};