function Engine(){
    var avatar = new Avatar();
    this.getAvatar = function(){
        return avatar;
    };
    this.reset = function(){
        this.getAvatar().reset();
    };
    let move = function(command){
        let direction = command.shift();
        let nextX = Directions.getNextX(direction, avatar.getX(), avatar.getY());
        let nextY = Directions.getNextY(direction, avatar.getX(), avatar.getY());
        //TODO: check for things that block movement!
        avatar.setX(nextX);
        avatar.setY(nextY);
    };
    this.doCommand = function(command){
        var token = command.shift();
        if(token===Commands.MOVE()){
            move(command);
        }
    };
}
