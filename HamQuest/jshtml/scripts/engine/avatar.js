function Avatar(){
    var x = 0;
    var y = 0;
    this.getX = function() { return x; };
    this.getY = function() { return y; };
    this.setX = function(newX) { x = newX; };
    this.setY = function(newY) { y = newY; };
    this.reset = function() {
        x = 0;
        y = 0;
    };
}