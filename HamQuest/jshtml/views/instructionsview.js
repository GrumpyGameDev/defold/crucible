function InstructionsView(view) {
    this.refresh = function () {
        var content = "<p>TODO: instructions</p>";
        content += "<button class=\"button is-fullwidth\" onclick=\"view.handleCommand('back')\">Main Menu</button>";
        Utility.updateDisplay(content);
    };
    this.handleCommand = function (command) {
        if (command === "back") {
            view.setCurrent(view.getViewNames().MAIN_MENU());
        }
    };
}