function View() {
    var viewNames = new ViewNames();
    var table = {};
    table[viewNames.MAIN_MENU()] = new MainMenuView(this)
    table[viewNames.INSTRUCTIONS()] = new InstructionsView(this)
    table[viewNames.NEUTRAL()] = new NeutralView(this)
    var current = viewNames.MAIN_MENU();
    var engine = new Engine();

    this.getEngine = function () {
        return engine;
    }
    this.refresh = function () {
        table[current].refresh();
    };
    this.getCurrent = function () {
        return current;
    };
    this.setCurrent = function (newCurrent) {
        current = newCurrent;
        this.refresh();
    };
    this.getViewNames = function () {
        return viewNames;
    };
    this.handleCommand = function (command) {
        table[current].handleCommand(command);
    };
    this.setCurrentFromEngineState = function(){
        this.setCurrent(viewNames.NEUTRAL());
    }

}