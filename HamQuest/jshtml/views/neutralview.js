function NeutralView(view) {
    this.refresh = function () {
        var content = "<p>Avatar Location: ("+view.getEngine().getAvatar().getX()+", "+view.getEngine().getAvatar().getY()+")</p>";
        content += "<div class=\"buttons\">";
        content += "<button class=\"button\" onclick=\"view.handleCommand('n')\">N</button>";
        content += "<button class=\"button\" onclick=\"view.handleCommand('e')\">E</button>";
        content += "<button class=\"button\" onclick=\"view.handleCommand('s')\">S</button>";
        content += "<button class=\"button\" onclick=\"view.handleCommand('w')\">W</button>";
        content += "</div>";
        Utility.updateDisplay(content);
    };
    this.handleCommand = function (command) {
        if(command==="n"){
            view.getEngine().doCommand([Commands.MOVE(), Directions.NORTH()]);
            view.refresh();
        }else if(command==="e"){
            view.getEngine().doCommand([Commands.MOVE(), Directions.EAST()]);
            view.refresh();
        }else if(command==="s"){
            view.getEngine().doCommand([Commands.MOVE(), Directions.SOUTH()]);
            view.refresh();
        }else if(command==="w"){
            view.getEngine().doCommand([Commands.MOVE(), Directions.WEST()]);
            view.refresh();
        }
    };
}