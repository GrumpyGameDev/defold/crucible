function MainMenuView(view) {
    this.refresh = function () {
        var content = "<p><button class=\"button is-fullwidth\" onclick=\"view.handleCommand('start')\">Play!</button></p>";
        content += "<p><button class=\"button is-fullwidth\" onclick=\"view.handleCommand('instructions')\">How?</button></p>";
        Utility.updateDisplay(content);
    };
    this.handleCommand = function (command) {
        if (command === "start") {
            view.getEngine().reset();
            view.setCurrent(view.getViewNames().NEUTRAL());
        } else if (command === "instructions") {
            view.setCurrent(view.getViewNames().INSTRUCTIONS());
        }
    };
}