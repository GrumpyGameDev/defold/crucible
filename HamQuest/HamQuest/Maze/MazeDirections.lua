local Direction=require("Common.Direction")

return {
	Direction.new("n","e","w","s",function(x,y) return x,y+1 end,0),
	Direction.new("e","s","n","w",function(x,y) return x+1,y end,0),
	Direction.new("s","w","e","n",function(x,y) return x,y-1 end,0),
	Direction.new("w","n","s","e",function(x,y) return x-1,y end,0)
}