local REFRESH = "refresh"
local REFRESH_ALL = "refreshAll"

local messageIds = {}

messageIds.REFRESH = function() return REFRESH end
messageIds.REFRESH_ALL = function() return REFRESH_ALL end

return messageIds