local FLOOR_OPEN = "openFloor"
local FLOOR_OPEN_DEADEND = "openFloorDeadend"
local FLOOR_EDGE = "edgeFloor"
local FLOOR_EDGE_DEADEND = "edgeFloorDeadend"
local FLOOR_ENTRANCE = "entranceFloor"
local FLOOR_ENTRANCE_DEADEND = "entranceFloorDeadend"
local SOLID = "entranceFloor"
local WALL_NORTH = "wallNorth"
local WALL_EAST = "wallEast"
local WALL_SOUTH = "wallSouth"
local WALL_WEST = "wallWest"
local DOOR_NORTH = "doorNorth"
local DOOR_EAST = "doorEast"
local DOOR_SOUTH = "doorSouth"
local DOOR_WEST = "doorWest"
local CORNER_INSIDE_NORTHEAST = "cornerInsideNortheast"
local CORNER_INSIDE_SOUTHEAST = "cornerInsideSoutheast"
local CORNER_INSIDE_SOUTHWEST = "cornerInsideSouthwest"
local CORNER_INSIDE_NORTHWEST = "cornerInsideNorthwest"
local CORNER_OUTSIDE_NORTHEAST = "cornerOutsideNortheast"
local CORNER_OUTSIDE_SOUTHEAST = "cornerOutsideSoutheast"
local CORNER_OUTSIDE_SOUTHWEST = "cornerOutsideSouthwest"
local CORNER_OUTSIDE_NORTHWEST = "cornerOutsideNorthwest"


local descriptors = {}
descriptors[FLOOR_OPEN]={}
descriptors[FLOOR_OPEN_DEADEND]={}
descriptors[FLOOR_EDGE]={}
descriptors[FLOOR_EDGE_DEADEND]={}
descriptors[FLOOR_ENTRANCE]={}
descriptors[FLOOR_ENTRANCE_DEADEND]={}
descriptors[SOLID]={}
descriptors[WALL_NORTH]={}
descriptors[WALL_EAST]={}
descriptors[WALL_SOUTH]={}
descriptors[WALL_WEST]={}
descriptors[DOOR_NORTH]={}
descriptors[DOOR_EAST]={}
descriptors[DOOR_SOUTH]={}
descriptors[DOOR_WEST]={}
descriptors[CORNER_INSIDE_NORTHEAST]={}
descriptors[CORNER_INSIDE_SOUTHEAST]={}
descriptors[CORNER_INSIDE_SOUTHWEST]={}
descriptors[CORNER_INSIDE_NORTHWEST]={}
descriptors[CORNER_OUTSIDE_NORTHEAST]={}
descriptors[CORNER_OUTSIDE_SOUTHEAST]={}
descriptors[CORNER_OUTSIDE_SOUTHWEST]={}
descriptors[CORNER_OUTSIDE_NORTHWEST]={}

--tiles
descriptors[FLOOR_OPEN].tile=1
descriptors[FLOOR_OPEN_DEADEND].tile=1
descriptors[FLOOR_EDGE].tile=1
descriptors[FLOOR_EDGE_DEADEND].tile=1
descriptors[FLOOR_ENTRANCE].tile=1
descriptors[FLOOR_ENTRANCE_DEADEND].tile=1
descriptors[SOLID].tile=2
descriptors[WALL_NORTH].tile=3
descriptors[WALL_EAST].tile=4
descriptors[WALL_SOUTH].tile=5
descriptors[WALL_WEST].tile=6
descriptors[DOOR_NORTH].tile=15
descriptors[DOOR_EAST].tile=16
descriptors[DOOR_SOUTH].tile=17
descriptors[DOOR_WEST].tile=18
descriptors[CORNER_INSIDE_NORTHEAST].tile=7
descriptors[CORNER_INSIDE_SOUTHEAST].tile=8
descriptors[CORNER_INSIDE_SOUTHWEST].tile=9
descriptors[CORNER_INSIDE_NORTHWEST].tile=10
descriptors[CORNER_OUTSIDE_NORTHEAST].tile=11
descriptors[CORNER_OUTSIDE_SOUTHEAST].tile=12
descriptors[CORNER_OUTSIDE_SOUTHWEST].tile=13
descriptors[CORNER_OUTSIDE_NORTHWEST].tile=14

--avatar spawning
descriptors[FLOOR_OPEN].spawnAvatar=true
descriptors[FLOOR_EDGE].spawnAvatar=true
for k, v in pairs(descriptors) do
	v.spawnAvatar = v.spawnAvatar or false
end

--avatar walking
descriptors[FLOOR_OPEN].walkAvatar=true
descriptors[FLOOR_OPEN_DEADEND].walkAvatar=true
descriptors[FLOOR_EDGE].walkAvatar=true
descriptors[FLOOR_EDGE_DEADEND].walkAvatar=true
descriptors[FLOOR_ENTRANCE].walkAvatar=true
descriptors[FLOOR_ENTRANCE_DEADEND].walkAvatar=true
descriptors[DOOR_NORTH].walkAvatar=true
descriptors[DOOR_EAST].walkAvatar=true
descriptors[DOOR_SOUTH].walkAvatar=true
descriptors[DOOR_WEST].walkAvatar=true
for k, v in pairs(descriptors) do
	v.walkAvatar = v.walkAvatar or false
end


local terrains = {}

terrains.FLOOR_OPEN = function() return FLOOR_OPEN end
terrains.FLOOR_OPEN_DEADEND = function() return FLOOR_OPEN_DEADEND end
terrains.FLOOR_EDGE = function() return FLOOR_EDGE end
terrains.FLOOR_EDGE_DEADEND = function() return FLOOR_EDGE_DEADEND end
terrains.FLOOR_ENTRANCE = function() return FLOOR_ENTRANCE end
terrains.FLOOR_ENTRANCE_DEADEND = function() return FLOOR_ENTRANCE_DEADEND end
terrains.SOLID = function() return SOLID end
terrains.WALL_NORTH = function() return WALL_NORTH end
terrains.WALL_EAST = function() return WALL_EAST end
terrains.WALL_SOUTH = function() return WALL_SOUTH end
terrains.WALL_WEST = function() return WALL_WEST end
terrains.DOOR_NORTH = function() return DOOR_NORTH end
terrains.DOOR_EAST = function() return DOOR_EAST end
terrains.DOOR_SOUTH = function() return DOOR_SOUTH end
terrains.DOOR_WEST = function() return DOOR_WEST end
terrains.CORNER_INSIDE_NORTHEAST = function() return CORNER_INSIDE_NORTHEAST end
terrains.CORNER_INSIDE_SOUTHEAST = function() return CORNER_INSIDE_SOUTHEAST end
terrains.CORNER_INSIDE_SOUTHWEST = function() return CORNER_INSIDE_SOUTHWEST end
terrains.CORNER_INSIDE_NORTHWEST = function() return CORNER_INSIDE_NORTHWEST end
terrains.CORNER_OUTSIDE_NORTHEAST = function() return CORNER_OUTSIDE_NORTHEAST end
terrains.CORNER_OUTSIDE_SOUTHEAST = function() return CORNER_OUTSIDE_SOUTHEAST end
terrains.CORNER_OUTSIDE_SOUTHWEST = function() return CORNER_OUTSIDE_SOUTHWEST end
terrains.CORNER_OUTSIDE_NORTHWEST = function() return CORNER_OUTSIDE_NORTHWEST end

terrains.getDescriptor = function(terrain) return descriptors[terrain] end
terrains.getTile = function(terrain) return terrains.getDescriptor(terrain).tile end

return terrains