local ROOM_COLUMNS = 15
local ROOM_ROWS = 15
local MAZE_COLUMNS = 8
local MAZE_ROWS = 8

local constants = {}

constants.ROOM_COLUMNS = function() return ROOM_COLUMNS end
constants.ROOM_ROWS = function() return ROOM_ROWS end
constants.MAZE_COLUMNS = function() return MAZE_COLUMNS end
constants.MAZE_ROWS = function() return MAZE_ROWS end

return constants