local gameTerrains = require("game.terrains")

local CREATURETYPE_AVATAR = "avatar"

local createBaseCreature = function(creatureType)
	local data = {}
	data.mazeColumn = 0
	data.mazeRow = 0
	data.roomColumn = 0
	data.roomRow = 0
	data.wounds = 0
	data.creatureType = creatureType
	data.isAvatar = true
	return data
end

local createAvatar = function(creatureType)
	local data = createBaseCreature(creatureType)
	return data
end

local nonDeadendSpawnTest = function(roomCell)
	return roomCell.creature==nil and roomCell.item==nil and gameTerrains.getDescriptor(roomCell.terrain).spawnAvatar
end

local avatarWalkTest = function(roomCell)
	return gameTerrains.getDescriptor(roomCell.terrain).walkAvatar
end

local descriptors = {}

descriptors[CREATURETYPE_AVATAR]={}
descriptors[CREATURETYPE_AVATAR].factory = createAvatar
descriptors[CREATURETYPE_AVATAR].spawnCount = 1
descriptors[CREATURETYPE_AVATAR].spawnTest = nonDeadendSpawnTest
descriptors[CREATURETYPE_AVATAR].walkTest = avatarWalkTest
descriptors[CREATURETYPE_AVATAR].tile = 51


local creatures = {}

creatures.CREATURETYPE_AVATAR = function() return CREATURETYPE_AVATAR end

creatures.getDescriptor = function(creatureType)
	return descriptors[creatureType]
end

creatures.getDescriptors = function()
	return descriptors
end

creatures.create = function(creatureType)
	return descriptors[creatureType].factory(creatureType)
end

creatures.getTile = function(creatureType)
	return descriptors[creatureType].tile
end

return creatures