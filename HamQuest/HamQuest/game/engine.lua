local gameConstants = require("game.constants")
local gameTerrains = require("game.terrains")
local mazeMaze = require("Maze.Maze")
local mazeGenerator = require("Maze.MazeGenerator")
local mazeDirections = require("Maze.MazeDirections")
local gameCreatures = require("game.creatures")

local COMMAND_NORTH = "north"
local COMMAND_EAST = "east"
local COMMAND_SOUTH = "south"
local COMMAND_WEST = "west"
local COMMAND_GREEN = "green"
local COMMAND_RED = "red"
local COMMAND_BLUE = "blue"
local COMMAND_YELLOW = "yellow"

local STATE_INGAME = "inGame"

local DIRECTION_NORTH = 1
local DIRECTION_EAST = 2
local DIRECTION_SOUTH = 3
local DIRECTION_WEST = 4

local data = {}

local getRoomCell = function(mazeColumn,mazeRow,roomColumn,roomRow)
	return data.world[mazeColumn][mazeRow][roomColumn][roomRow]
end

local resetWorld = function()
	data.world={}
	for mazeColumn = 1, gameConstants.MAZE_COLUMNS() do
		data.world[mazeColumn]={}
		for mazeRow = 1, gameConstants.MAZE_ROWS() do
			data.world[mazeColumn][mazeRow]={}
			for roomColumn = 1, gameConstants.ROOM_COLUMNS() do
				data.world[mazeColumn][mazeRow][roomColumn]={}
				for roomRow = 1, gameConstants.ROOM_ROWS() do
					data.world[mazeColumn][mazeRow][roomColumn][roomRow]={}
					data.world[mazeColumn][mazeRow][roomColumn][roomRow].terrain=gameTerrains.FLOOR_OPEN()
				end
			end
		end
	end
end

local resetCreatures = function()
	data.creatures = {n=0}
end

local applyMazeCell = function (mazeCell,room)
	local openFloorTerrain = gameTerrains.FLOOR_OPEN()
	local edgeFloorTerrain = gameTerrains.FLOOR_EDGE()
	local entranceFloorTerrain = gameTerrains.FLOOR_ENTRANCE()

	if #(mazeCell:getOpenDoors()) == 1 then
		openFloorTerrain = gameTerrains.FLOOR_OPEN_DEADEND()
		edgeFloorTerrain = gameTerrains.FLOOR_EDGE_DEADEND()
		entranceFloorTerrain = gameTerrains.FLOOR_ENTRANCE_DEADEND()
	end

	for column = 1, gameConstants.ROOM_COLUMNS() do
		for row = 1, gameConstants.ROOM_ROWS() do
			local roomCell = room[column][row]
			if column==1 or row==1 or column == gameConstants.ROOM_COLUMNS() or row == gameConstants.ROOM_ROWS() then
				roomCell.terrain = entranceFloorTerrain
			elseif column==2 or row==2 or column == gameConstants.ROOM_COLUMNS()-1 or row == gameConstants.ROOM_ROWS()-1 then
				roomCell.terrain = edgeFloorTerrain
			else
				roomCell.terrain = openFloorTerrain
			end
		end
	end
		
	room[1][1].terrain = gameTerrains.CORNER_INSIDE_SOUTHWEST()
	room[1][gameConstants.ROOM_ROWS()].terrain = gameTerrains.CORNER_INSIDE_NORTHWEST()
	room[gameConstants.ROOM_COLUMNS()][1].terrain = gameTerrains.CORNER_INSIDE_SOUTHEAST()
	room[gameConstants.ROOM_COLUMNS()][gameConstants.ROOM_ROWS()].terrain = gameTerrains.CORNER_INSIDE_NORTHEAST()

	for column=2,gameConstants.ROOM_COLUMNS()-1 do
		room[column][1].terrain = gameTerrains.WALL_SOUTH()
		room[column][gameConstants.ROOM_ROWS()].terrain = gameTerrains.WALL_NORTH()
	end

	for row=2,gameConstants.ROOM_ROWS()-1 do
		room[1][row].terrain = gameTerrains.WALL_WEST()
		room[gameConstants.ROOM_COLUMNS()][row].terrain = gameTerrains.WALL_EAST()
	end

	local mazeDoor = mazeCell:getDoor("n")
	if mazeDoor~=nil and mazeDoor:isOpen() then
		room[(gameConstants.ROOM_COLUMNS()+1)/2][gameConstants.ROOM_ROWS()].terrain = gameTerrains.DOOR_NORTH()
	end
	mazeDoor = mazeCell:getDoor("e")
	if mazeDoor~=nil and mazeDoor:isOpen() then
		room[gameConstants.ROOM_COLUMNS()][(gameConstants.ROOM_ROWS()+1)/2].terrain = gameTerrains.DOOR_EAST()
	end
	mazeDoor = mazeCell:getDoor("s")
	if mazeDoor~=nil and mazeDoor:isOpen() then
		room[(gameConstants.ROOM_COLUMNS()+1)/2][1].terrain = gameTerrains.DOOR_SOUTH()
	end
	mazeDoor = mazeCell:getDoor("w")
	if mazeDoor~=nil and mazeDoor:isOpen() then
		room[1][(gameConstants.ROOM_ROWS()+1)/2].terrain = gameTerrains.DOOR_WEST()
	end
end

local applyMaze = function()
	local maze = mazeMaze.new(gameConstants.MAZE_COLUMNS(), gameConstants.MAZE_ROWS(), mazeDirections)
	mazeGenerator.generate(maze, mazeDirections)
	for mazeColumn = 1, gameConstants.MAZE_COLUMNS() do
		for mazeRow = 1, gameConstants.MAZE_ROWS() do
			local room = data.world[mazeColumn][mazeRow]
			local mazeCell = maze:getCell(mazeColumn,mazeRow)
			applyMazeCell(mazeCell,room)
		end
	end
end

local checkRoomSpawnable = function(mazeColumn,mazeRow,spawnTest)
	for roomColumn=1,gameConstants.ROOM_COLUMNS() do
		for roomRow = 1, gameConstants.ROOM_ROWS() do
			if spawnTest(getRoomCell(mazeColumn,mazeRow,roomColumn,roomRow)) then
				return true
			end
		end
	end
	return false
end

local addCreatureInstance = function(creature)
	for index = 1, data.creatures.n do
		if data.creatures[index]==nil then
			data.creatures[index]=creature
			return index
		end
	end	
	data.creatures.n = data.creatures.n + 1
	data.creatures[data.creatures.n]=creature
	return data.creatures.n
end

local placeCreatureInstance = function(mazeColumn,mazeRow,roomColumn,roomRow,instance)
	getRoomCell(mazeColumn, mazeRow, roomColumn, roomRow).creature = instance
end

local placeCreature = function(creature, instance)
	placeCreatureInstance(creature.mazeColumn, creature.mazeRow, creature.roomColumn, creature.roomRow, instance)
end

local spawnCreature = function(creatureType, descriptor)
	local mazeColumn=0
	local mazeRow=0
	repeat
		mazeColumn = math.random(1,gameConstants.MAZE_COLUMNS())
		mazeRow = math.random(1,gameConstants.MAZE_ROWS())
	until checkRoomSpawnable(mazeColumn, mazeRow, descriptor.spawnTest)
	local roomColumn
	local roomRow
	repeat
		roomColumn = math.random(1,gameConstants.ROOM_COLUMNS())
		roomRow = math.random(1,gameConstants.ROOM_ROWS())
	until descriptor.spawnTest(getRoomCell(mazeColumn,mazeRow,roomColumn,roomRow))
	local creature = descriptor.factory(creatureType)
	local instance = addCreatureInstance(creature)
	creature.mazeColumn = mazeColumn
	creature.mazeRow = mazeRow
	creature.roomColumn = roomColumn
	creature.roomRow = roomRow
	placeCreatureInstance(mazeColumn,mazeRow,roomColumn,roomRow,instance)
end

local findAvatarInstance = function()
	for i,v in ipairs(data.creatures) do
		if v.isAvatar then
			return i
		end
	end
	return nil
end

local spawnCreatures = function()
	for k, v in pairs(gameCreatures.getDescriptors()) do
		for index = 1, v.spawnCount do
			spawnCreature(k,v)
		end
	end
	data.avatarInstance = findAvatarInstance()
end

local resetData = function()
	data = {}
	data.state = STATE_INGAME
end

local reset = function()
	math.randomseed(os.time())
	
	resetData()
	resetWorld()
	resetCreatures()
	
	applyMaze()
	spawnCreatures()
end

local moveAvatar = function(direction)
	local avatarCreature = data.creatures[data.avatarInstance]
	placeCreature(avatarCreature, nil)
	local direction = mazeDirections[direction]
	local nextRoomColumn, nextRoomRow = direction:step(avatarCreature.roomColumn, avatarCreature.roomRow)
	local nextMazeColumn = avatarCreature.mazeColumn
	local nextMazeRow = avatarCreature.mazeRow
	if nextRoomColumn<1 then
		nextMazeColumn = nextMazeColumn - 1
		nextRoomColumn = gameConstants.ROOM_COLUMNS()
	elseif nextRoomColumn>gameConstants.ROOM_COLUMNS() then
		nextMazeColumn = nextMazeColumn + 1
		nextRoomColumn = 1
	end
	if nextRoomRow<1 then
		nextMazeRow = nextMazeRow - 1
		nextRoomRow=gameConstants.ROOM_ROWS()
	elseif nextRoomRow>gameConstants.ROOM_ROWS() then
		nextMazeRow = nextMazeRow + 1
		nextRoomRow = 1
	end
	if nextMazeColumn>=1 and nextMazeColumn<=gameConstants.MAZE_COLUMNS() and nextMazeRow>=1 and nextMazeRow<=gameConstants.MAZE_ROWS() then
		local nextRoomCell = getRoomCell(nextMazeColumn, nextMazeRow, nextRoomColumn, nextRoomRow)
		local creatureDescriptor = gameCreatures.getDescriptor(avatarCreature.creatureType)
		if creatureDescriptor.walkTest(nextRoomCell) then
			avatarCreature.mazeColumn = nextMazeColumn
			avatarCreature.mazeRow = nextMazeRow
			avatarCreature.roomColumn = nextRoomColumn
			avatarCreature.roomRow = nextRoomRow
		end
	end
	placeCreature(avatarCreature, data.avatarInstance)
end

local inGameCommandHandler = function(command)
	if command==COMMAND_NORTH then
		moveAvatar(DIRECTION_NORTH)
	elseif command==COMMAND_EAST then
		moveAvatar(DIRECTION_EAST)
	elseif command==COMMAND_SOUTH then
		moveAvatar(DIRECTION_SOUTH)
	elseif command==COMMAND_WEST then
		moveAvatar(DIRECTION_WEST)
	end
end

local stateCommandHandlers = {}
stateCommandHandlers[STATE_INGAME] = inGameCommandHandler

local engine = {}

engine.reset = reset

engine.getRoomCell = function(mazeColumn,mazeRow,roomColumn,roomRow)
	return getRoomCell(mazeColumn,mazeRow,roomColumn,roomRow)
end

engine.getAvatarMazePosition = function()
	local creature = data.creatures[data.avatarInstance]
	return creature.mazeColumn, creature.mazeRow
end

engine.getCreature = function(instance)
	return data.creatures[instance]
end

engine.doCommand = function(command)
	stateCommandHandlers[data.state](command)
end

engine.COMMAND_NORTH = function() return COMMAND_NORTH end
engine.COMMAND_EAST = function() return COMMAND_EAST end
engine.COMMAND_SOUTH = function() return COMMAND_SOUTH end
engine.COMMAND_WEST = function() return COMMAND_WEST end
engine.COMMAND_GREEN = function() return COMMAND_GREEN end
engine.COMMAND_RED = function() return COMMAND_RED end
engine.COMMAND_BLUE = function() return COMMAND_BLUE end
engine.COMMAND_YELLOW = function() return COMMAND_YELLOW end

return engine