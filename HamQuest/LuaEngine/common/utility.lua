local module = {}

module.split = function(text, splitter)
    local result = {}
    for token in string.gmatch(text, splitter) do
        table.insert(result, token)
    end
    return result
end

return module