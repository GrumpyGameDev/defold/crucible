local COMMAND_MOVE=function() return "move" end

local MOVE_NORTH = function() return "north" end
local MOVE_EAST = function() return "east" end
local MOVE_SOUTH = function() return "south" end
local MOVE_WEST = function() return "west" end

local module = {}

module.COMMAND_MOVE = COMMAND_MOVE

module.MOVE_NORTH = MOVE_NORTH
module.MOVE_EAST = MOVE_EAST
module.MOVE_SOUTH = MOVE_SOUTH
module.MOVE_WEST = MOVE_WEST


return module