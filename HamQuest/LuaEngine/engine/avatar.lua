local module = {}

module.new = function()
    local instance = {}

    local atlasColumn = 1
    local atlasRow = 1
    local mapColumn = 1
    local mapRow = 1

    function instance:getAtlasColumn()
        return atlasColumn
    end
    function instance:setAtlasColumn(value)
        atlasColumn = value
    end
    function instance:getAtlasRow()
        return atlasRow
    end
    function instance:setAtlasRow(value)
        atlasRow = value
    end
    function instance:getMapColumn()
        return mapColumn
    end
    function instance:setMapColumn(value)
        mapColumn = value
    end
    function instance:getMapRow()
        return mapRow
    end
    function instance:setMapRow(value)
        mapRow = value
    end
    function instance:serialize()
        return {
            atlasColumn = atlasColumn,
            atlasRow = atlasRow,
            mapColumn = mapColumn,
            mapRow = mapRow
        }
    end
    function instance:deserialize(import)
        atlasColumn = import.atlasColumn
        atlasRow = import.atlasRow
        mapColumn = import.mapColumn
        mapRow = import.mapRow
    end

    return instance
end

return module