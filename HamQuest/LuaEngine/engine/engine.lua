local engineAvatar = require("engine.avatar")
local directions = require("engine.directions")
local commands = require("engine.commands")
local constants = require("engine.constants")

local module = {}

module.new = function()
    local instance = {}

    local avatar = engineAvatar.new()

    function instance:getAvatar()
        return avatar
    end

    function instance:start()
    end

    function instance:finish()
    end

    function instance:boot()
        avatar = engineAvatar.new()
    end

    function instance:serialize()
        return {
            avatar = avatar.serialize()
        }
    end

    function instance:deserialize(import)
        avatar:deserialize(import.avatar)
    end

    local directionTable = {}
    directionTable[commands.MOVE_NORTH()] = 1
    directionTable[commands.MOVE_EAST()] = 2
    directionTable[commands.MOVE_SOUTH()] = 3
    directionTable[commands.MOVE_WEST()] = 4

    function doMoveCommand(command)
        if #command>0 then
            local direction = directionTable[table.remove(command,1)]
            if direction~=nil then
                local nextMapColumn
                local nextMapRow
                nextMapColumn, nextMapRow = (directions[direction]).step(avatar.getMapColumn(), avatar.getMapRow())
                print(nextMapColumn, nextMapRow)
                local nextAtlasColumn = avatar.getAtlasColumn()
                local nextAtlasRow = avatar.getAtlasRow()
                while nextMapColumn<1 do
                    nextMapColumn = nextMapColumn + constants.MAP_COLUMNS()
                    nextAtlasColumn = nextAtlasColumn - 1
                end
                while nextMapColumn>constants.MAP_COLUMNS() do
                    nextMapColumn = nextMapColumn - constants.MAP_COLUMNS()
                    nextAtlasColumn = nextAtlasColumn + 1
                end
                while nextMapRow<1 do
                    nextMapRow = nextMapRow + constants.MAP_ROWS()
                    nextAtlasRow = nextAtlasRow - 1
                end
                while nextMapRow>constants.MAP_ROWS() do
                    nextMapRow = nextMapRow - constants.MAP_ROWS()
                    nextAtlasRow = nextAtlasRow + 1
                end
                while nextAtlasColumn<1 do
                    nextAtlasColumn = nextAtlasColumn + constants.ATLAS_COLUMNS()
                end
                while nextAtlasColumn>constants.ATLAS_COLUMNS() do
                    nextAtlasColumn = nextAtlasColumn - constants.ATLAS_COLUMNS()
                end
                while nextAtlasRow<1 do
                    nextAtlasRow = nextAtlasRow + constants.ATLAS_ROWS()
                end
                while nextAtlasRow>constants.ATLAS_ROWS() do
                    nextAtlasRow = nextAtlasRow - constants.ATLAS_ROWS()
                end

                avatar.setAtlasColumn(nextAtlasColumn)
                avatar.setAtlasRow(nextAtlasRow)
                avatar.setMapColumn(nextMapColumn)
                avatar.setMapRow(nextMapRow)
            end
        end
    end

    function instance:doCommand(command)
        if #command>0 then
            local token = table.remove(command,1)
            if token==commands.COMMAND_MOVE() then
                doMoveCommand(command)
            end
        end
    end

    return instance
end

return module