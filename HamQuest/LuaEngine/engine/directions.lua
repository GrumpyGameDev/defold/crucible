local engineConstants = require("engine.constants")
local direction = require("common.direction")

return {
    direction.new(engineConstants.DIRECTION_NORTH(),engineConstants.DIRECTION_EAST(),engineConstants.DIRECTION_WEST(),engineConstants.DIRECTION_SOUTH(),function(x,y) return x,y+1 end),
    direction.new(engineConstants.DIRECTION_EAST(),engineConstants.DIRECTION_SOUTH(),engineConstants.DIRECTION_NORTH(),engineConstants.DIRECTION_WEST(),function(x,y) return x+1,y end),
    direction.new(engineConstants.DIRECTION_SOUTH(),engineConstants.DIRECTION_WEST(),engineConstants.DIRECTION_EAST(),engineConstants.DIRECTION_NORTH(),function(x,y) return x,y-1 end),
    direction.new(engineConstants.DIRECTION_WEST(),engineConstants.DIRECTION_NORTH(),engineConstants.DIRECTION_SOUTH(),engineConstants.DIRECTION_EAST(),function(x,y) return x-1,y end)
}