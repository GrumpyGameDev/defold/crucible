local ATLAS_COLUMNS = function() return 8 end
local ATLAS_ROWS = function() return 8 end
local MAP_COLUMNS = function() return 15 end
local MAP_ROWS = function() return 15 end
local DIRECTION_NORTH = function() return "north" end
local DIRECTION_EAST = function() return "east" end
local DIRECTION_SOUTH = function() return "south" end
local DIRECTION_WEST = function() return "west" end

local module = {}

module.ATLAS_COLUMNS = ATLAS_COLUMNS
module.ATLAS_ROWS = ATLAS_ROWS
module.MAP_COLUMNS = MAP_COLUMNS
module.MAP_ROWS = MAP_ROWS
module.DIRECTION_NORTH = DIRECTION_NORTH
module.DIRECTION_EAST = DIRECTION_EAST
module.DIRECTION_SOUTH = DIRECTION_SOUTH
module.DIRECTION_WEST = DIRECTION_WEST

return module