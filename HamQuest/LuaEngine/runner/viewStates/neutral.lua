local runnerStates = require("runner.states")
local utility = require("common.utility")
local engineConstants = require("engine.constants")
local engineCommands = require("engine.commands")

local TOKEN_QUIT = "q"
local TOKEN_NORTH = "n"
local TOKEN_EAST = "e"
local TOKEN_SOUTH = "s"
local TOKEN_WEST = "w"

local module = {}

module.new = function()
    local instance = {}

    function handleQuit()
        return nil
    end

    function handleInput(engine, tokens)
        local token = table.remove(tokens,1)
        if token==TOKEN_QUIT then
            return handleQuit()
        elseif token==TOKEN_NORTH then
            engine:doCommand({engineCommands.COMMAND_MOVE(), engineCommands.MOVE_NORTH()})
            return runnerStates.STATE_NEUTRAL
        elseif token==TOKEN_EAST then
            engine:doCommand({engineCommands.COMMAND_MOVE(), engineCommands.MOVE_EAST()})
            return runnerStates.STATE_NEUTRAL
        elseif token==TOKEN_SOUTH then
            engine:doCommand({engineCommands.COMMAND_MOVE(), engineCommands.MOVE_SOUTH()})
            return runnerStates.STATE_NEUTRAL
        elseif token==TOKEN_WEST then
            engine:doCommand({engineCommands.COMMAND_MOVE(), engineCommands.MOVE_WEST()})
            return runnerStates.STATE_NEUTRAL
        else
            print("Invalid command!")
            return runnerStates.STATE_NEUTRAL
        end
    end

    function refreshView(engine)
        local avatar = engine:getAvatar()
        print("Map: "..avatar:getAtlasColumn()..","..avatar:getAtlasRow())
        for mapRow = 1, engineConstants.MAP_ROWS() do
            local line = ""
            for mapColumn = 1, engineConstants.MAP_COLUMNS() do
                if mapRow==avatar:getMapRow() and mapColumn==avatar:getMapColumn() then
                    line = line .. "@"
                else
                    line = line .. "."
                end
            end
            print(line)
        end
    end

    function instance:execute(engine)
        refreshView(engine)
        io.write(">")
        local input = io.read()
        local tokens = utility.split(input, "%S+")
        return handleInput(engine, tokens)
    end
    return instance
end

return module