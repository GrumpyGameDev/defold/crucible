local runnerStates = require("runner.states")

local module = {}

module.new = function()
    local instance = {}
    function instance:execute(engine)
        print("boot")
        return runnerStates.STATE_NEUTRAL
    end
    return instance
end

return module