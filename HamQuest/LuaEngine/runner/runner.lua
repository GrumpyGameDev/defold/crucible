local engine = require("engine.engine")
local runnerStates = require("runner.states")

local execute = function(engine)
    local states = {}
    states[runnerStates.STATE_BOOT] = (require("runner.viewStates.boot")).new()
    states[runnerStates.STATE_NEUTRAL] = (require("runner.viewStates.neutral")).new()

    local currentState = runnerStates.STATE_BOOT
    while currentState~=nil do
        currentState = states[currentState]:execute(engine)
    end
end

local module = {}

module.run = function()
    local instance = engine.new()
    instance:start()
    execute(instance)
    instance:finish()
end

return module