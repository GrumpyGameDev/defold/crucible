local STATE_BOOT = "boot"
local STATE_NEUTRAL = "neutral"

return {
    STATE_BOOT = STATE_BOOT,
    STATE_NEUTRAL = STATE_NEUTRAL
}