local controllerCommands = require("controller.commands")
local model = require("model.model")
local module = {}

function module.doCommand(command, parameters)
    if command==controllerCommands.COMMAND_RESET() then
        model.reset()
    end
end

return module
