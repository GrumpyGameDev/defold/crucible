local viewState = require("view.state")
local currentView = viewState.VIEW_NONE()
local views = {}
views[viewState.VIEW_MAINMENU()] = require("view.mainmenu")
views[viewState.VIEW_ATSEA()] = require("view.atsea")

local function startCurrentView()
    local view = views[currentView]
    if view~=nil and view.start~=nil then
        view.start()
    end
end

local function finishCurrentView()
    local view = views[currentView]
    if view~=nil and view.finish~=nil then
        view.finish()
    end
end

local function setCurrentView(newView)
    finishCurrentView()
    currentView = newView
    startCurrentView()
end

local function drawCurrentView()
    local view = views[currentView]
    if view~=nil and view.draw~=nil then
        view.draw()
    end
end

local function updateCurrentView(command)
    local view = views[currentView]
    if view~=nil and view.update~=nil then
        local nextView = view.update(command)
        if nextView~=nil then
            setCurrentView(nextView)
        end
    end
end

local module = {}

function module.getCurrentView()
    return currentView
end

function module.setCurrentView(newView)
    setCurrentView(newView)
end

function module.draw()
    drawCurrentView()
end

function module.update(command)
    updateCurrentView(command)
end

return module