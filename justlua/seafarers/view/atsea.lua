local viewState = require("view.state")
local model = require("model.model")
local VIEWCOMMAND_QUIT = "q"
local VIEWCOMMAND_MOVE = "m"
local VIEWCOMMAND_HEADING = "h"
local VIEWCOMMAND_SPEED = "s"

local module = {}

function module.draw()
    print("At Sea:")
    print("X: "..model.getAvatar():getX())
    print("Y: "..model.getAvatar():getY())
    print("Heading: "..model.getAvatar():getHeading())
    print("Speed: "..model.getAvatar():getSpeed())
    print("[M]ove")
    print("Change [H]eading")
    print("Change [S]peed")
    print("[Q]uit")
end

local function doMove()
    print("move")
end

local function changeHeading(command)
    print("heading")
end

local function changeSpeed(command)
    print("speed")
end

function module.update(command)
    local token = table.remove(command,1):lower()
    if token==VIEWCOMMAND_QUIT then
        return viewState.VIEW_NONE()
    elseif token==VIEWCOMMAND_MOVE then
        doMove()
    elseif token==VIEWCOMMAND_HEADING then
        changeHeading(command)
    elseif token==VIEWCOMMAND_SPEED then
        changeSpeed(command)
    end
end

return module
