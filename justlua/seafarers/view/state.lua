local VIEW_NONE = "none"
local VIEW_MAINMENU = "main-menu"
local VIEW_ATSEA = "at-sea"

local module = {}

function module.VIEW_NONE() return VIEW_NONE end
function module.VIEW_MAINMENU() return VIEW_MAINMENU end
function module.VIEW_ATSEA() return VIEW_ATSEA end

return  module