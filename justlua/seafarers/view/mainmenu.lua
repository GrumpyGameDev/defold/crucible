local viewState = require("view.state")
local controllerCommands = require("controller.commands")
local controller = require("controller.controller")
local VIEWCOMMAND_QUIT = "q"
local VIEWCOMMAND_NEW = "n"

local module = {}

function module.draw()
    print()
    print("Main Menu:")
    print("[N]ew")
    print("[Q]uit")
end

function module.update(command)
    local token = table.remove(command,1):lower()
    if token==VIEWCOMMAND_QUIT then
        return viewState.VIEW_NONE()
    elseif token==VIEWCOMMAND_NEW then
        controller.doCommand(controllerCommands.COMMAND_RESET())
        return viewState.VIEW_ATSEA()
    else
        print("MainMenu: Unknown command!")
    end
end

function module.setController(newController)
    controller = newController
end

return module