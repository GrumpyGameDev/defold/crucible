local function splitString (inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

local module = {}

function module.splitString(inputstr, sep)
    return splitString(inputstr,sep)
end

function module.deepCopy(original)
    return deepCopy(original)
end

return module