local module = {}

function module.new(instanceData)
    instanceData.x = 0
    instanceData.y = 0
    instanceData.heading = 0
    instanceData.speed = 1

    local instance = {}

    function instance:getX()
        return instanceData.x
    end
    function instance:getY()
        return instanceData.y
    end
    function instance:getHeading()
        return instanceData.heading
    end
    function instance:getSpeed()
        return instanceData.speed
    end

    function instance:setX(x)
        instanceData.x = x
    end
    function instance:setY(y)
        instanceData.y = y
    end
    function instance:setHeading(heading)
        instanceData.heading = heading
    end
    function instance:setSpeed(speed)
        instanceData.speed = speed
    end
    
    return instance
end

return module
