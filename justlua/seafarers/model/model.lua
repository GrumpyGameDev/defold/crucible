local model = nil
local modelAvatar = require("model.avatar")
local avatar = nil

local module = {}

function module.reset()
    model = {}
    model.avatar = {}
    avatar = modelAvatar.new(model.avatar)
end

function module.getAvatar()
    return avatar
end


return module