local viewState = require("view.state")
local view = require("view.view")
local utility = require("utility.utility")

local function main()
    view.setCurrentView(viewState.VIEW_MAINMENU())
    repeat
        view.draw()
        local input = io.read("*line")
        local tokens = utility.splitString(input)
        if #tokens>0 then
            view.update(tokens)
        end
    until view.getCurrentView()==viewState.VIEW_NONE()
end
main()
