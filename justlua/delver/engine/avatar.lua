local module = {}

function module.new(instanceData)
    local instance = {}
    
    function instance:getAtlasColumn()
      return instance.atlasColumn
    end
    
    function instance:setAtlasColumn(value)
      instance.atlasColumn = value
    end
    
    function instance:getAtlasRow()
      return instance.atlasRow
    end
    
    function instance:setAtlasRow(value)
      instance.atlasRow = value
    end
    
    function instance:getMapColumn()
      return instance.mapColumn
    end
    
    function instance:setMapColumn(value)
      instance.mapColumn = value
    end
    
    function instance:getMapRow()
      return instance.mapRow
    end
    
    function instance:setMapRow(value)
      instance.mapRow = value
    end
    
    function instance:getCreatureIndex()
      return instance.creatureIndex
    end

    function instance:setCreatureIndex(value)
      instance.creatureIndex = value
    end

    return instance
end

return module
