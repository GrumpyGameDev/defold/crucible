local mapColumn = require("engine.mapcolumn")
local module = {}

function module.new(instanceData,mapColumns,mapRows)
    local mapColumnInstances = {}
    for column = 1, mapColumns do
        if instanceData[column]==nil then
            instanceData[column]={}
        end
        mapColumnInstances[column]=mapColumn.new(instanceData[column], mapRows)
    end
    local instance = {}
    function instance:getMapColumns()
        return mapColumns
    end
    function instance:getMapRows()
        return mapRows
    end
    function instance:getMapColumn(column)
        return mapColumnInstances[column]
    end
    return instance
end

return module