local constants = require("engine.constants")
local atlas = require("engine.atlas")
local avatar = require("engine.avatar")

local module = {}

function module.new(instanceData)
    if instanceData==nil then
        instanceData = {}
    end

    local instance = {}
    function instance:getConstants()
        return constants
    end

    if instanceData.atlas==nil then
        instanceData.atlas = {}
    end
    local atlas = atlas.new(instanceData.atlas, constants.ATLAS_COLUMNS(), constants.ATLAS_ROWS(), constants.MAP_COLUMNS(), constants.MAP_ROWS())
    function instance:getAtlas()
        return atlas
    end

    if instanceData.avatar==nil then
        instanceData.avatar = {}
    end
    local avatarInstance = avatar.new(instanceData.avatar)
    function instance:getAvatar()
        return avatarInstance
    end

    return instance
end

return module