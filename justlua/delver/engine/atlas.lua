local atlasColumn = require("engine.atlascolumn")
local module = {}

function module.new(instanceData,atlasColumns,atlasRows,mapColumns,mapRows)
    local atlasColumnInstances = {}
    for column = 1, atlasColumns do
        if instanceData[column]==nil then
            instanceData[column]={}
        end
        atlasColumnInstances[column]=atlasColumn.new(instanceData[column],atlasRows,mapColumns,mapRows)
    end
    local instance = {}
    function instance:getAtlasColumns()
        return atlasColumns
    end
    function instance:getAtlasRows()
        return atlasRows
    end
    function instance:getMapColumns()
        return mapColumns
    end
    function instance:getMapRows()
        return mapRows
    end
    function instance:getAtlasColumn(column)
        return atlasColumnInstances[column]
    end
    return instance
end

return module