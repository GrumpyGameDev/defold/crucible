local mapCell = require("engine.mapcell")
local module = {}

function module.new(instanceData,mapRows)
    local mapCellInstances = {}
    for row = 1, mapRows do
        if instanceData[row]==nil then
            instanceData[row]={}
        end
        mapCellInstances[row]=mapCell.new(instanceData[row])
    end
    local instance = {}
    function instance:getMapRows()
        return mapRows
    end
    function instance:getMapCell(row)
        return mapCellInstances[row]
    end
    return instance
end

return module