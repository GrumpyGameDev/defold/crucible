local map = require("engine.map")
local module = {}

function module.new(instanceData,atlasRows,mapColumns,mapRows)
    local mapInstances = {}
    for row = 1, atlasRows do
        if instanceData[row]==nil then
            instanceData[row]={}
        end
        mapInstances[row]=map.new(instanceData[row], mapColumns, mapRows)
    end
    local instance = {}
    function instance:getAtlasRows()
        return atlasRows
    end
    function instance:getMapColumns()
        return mapColumns
    end
    function instance:getMapRows()
        return mapRows
    end
    function instance:getMap(row)
        return mapInstances[row]
    end
    return instance
end

return module