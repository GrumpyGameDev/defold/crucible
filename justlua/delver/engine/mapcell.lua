local module = {}

function module.new(instanceData)
    local instance = {}
    function instance:getTerrain()
        return instanceData.terrain
    end
    function instance:getItem()
        return instanceData.item
    end
    function instance:getCreature()
        return instanceData.creature
    end
    function instance:getEffect()
        return instanceData.effect
    end
    function instance:setTerrain(terrain)
        instanceData.terrain = terain
    end
    function instance:setItem(item)
        instanceData.item = item
    end
    function instance:setCreature(creature)
        instanceData.creature = creature
    end
    function instance:setEffect(effect)
        instanceData.effect = effect
    end
    return instance
end

return module