local MAP_COLUMNS = function() return 15 end
local MAP_ROWS = function() return 15 end
local ATLAS_COLUMNS = function() return 8 end
local ATLAS_ROWS = function() return 8 end

local module = {}

module.MAP_COLUMNS=MAP_COLUMNS
module.MAP_ROWS=MAP_ROWS
module.ATLAS_COLUMNS=ATLAS_COLUMNS
module.ATLAS_ROWS=ATLAS_ROWS

return module