local engine = require("engine.engine")
local runner = require("runner.runner")

function main()
    local instance = runner.new(engine.new())
    instance:run()
end
main()