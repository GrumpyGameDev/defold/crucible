local MINIMUM = 1
local MAXIMUM = 100
math.randomseed(os.time())
local target = math.random(MINIMUM, MAXIMUM)
local guessCount = 0
local done = false
while not done do
    print("Guess my number "..MINIMUM.." to "..MAXIMUM..".")
    local guess = tonumber(io.read("*line"))
    if guess==nil then
        print("That is not a number.")
    elseif guess<MINIMUM or guess>MAXIMUM then
        print("That number is not from "..MINIMUM.." to "..MAXIMUM..".")
    elseif guess>target then
        print("Too high!")
    elseif guess<target then
        print("Too high!")
    else
    end
end