# Tables

* arg
  * arg[1] thru arg[n]: variable args
  * arg.n: number of variable args
  * ex: function x(...)  end
* bit32
  * bit32.arshift(x,disp)
  * bit32.band(...)
  * bit32.bnot(x)
  * bit32.bor(...)
  * bit32.btest(...)
  * bit32.bxor(...)
  * bit32.extract(, field, _width_)
  * bit32.lrotate(x, disp)
  * bit32.lshift(x, disp)
  * bit32.replace(n, v, field, _width_)
  * bit32.rrotate(x, disp)
  * bit32.rshift(x, disp)
* coroutine
  * coroutine.create
  * coroutine.resume
  * coroutine.running
  * coroutine.status
  * coroutine.wrap
  * coroutine.yield
* debug:table
* io:table
* math:table
* os:table
* package:table
* string:table
* table:table
* _G:table

# Functions

* assert:function
* collectgarbage:function
* dofile:function
* error:function
* getmetatable:function
* ipairs:function
* load:function
* loadfile:function
* loadstring:function
* module:function
* next:function
* pairs:function
* pcall:function
* print:function
* rawequal:function
* rawget:function
* rawlen:function
* rawset:function
* require:function
* select:function
* setmetatable:function
* tonumber:function
* tostring:function
* type:function
* unpack:function
* xpcall:function

# Other

* _VERSION:string
