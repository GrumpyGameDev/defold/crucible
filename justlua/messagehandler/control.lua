local messageHandler = require("messagehandler")
local module = {}

module.new = function(parent,x,y,width,height,enabled)
    local instance = messageHandler.new(parent)
    instance.x = x
    instance.y = y
    instance.width = width
    instance.height = height
    instance.enabled = enabled
    function instance:getX() return self.x end
    function instance:getY() return self.y end
    function instance:getWidth() return self.width end
    function instance:getHeight() return self.height end
    function instance:isEnabled() return self.enabled end
    function instance:setX(x) self.x = x end
    function instance:setY(y) self.y = y end
    function instance:setWidth(width) self.width = width end
    function instance:setHeight(height) self.height = height end
    function instance:setEnabled(enabled) self.enabled = enabled end
    return instance
end

return module