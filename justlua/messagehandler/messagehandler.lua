local module = {}

module.new = function (parent)
    local instance = {}
    instance.children = {}
    function instance:getParent() return self.parent end
    function instance:hasParent() return self.parent~=nil end
    function instance:setParent(parent) 
        if self:hasParent() then
            self.parent:removeChild(self)
        end
        self.parent = parent
        if self:hasParent() then
            self.parent:addChild(self)
        end
    end
    function instance:addChild(child)
        self:removeChild(child)
        table.insert(self.children)
    end
    function instance:removeChild(child)
        local indices = {}
        for i,v in ipairs(self.children) do
            if v==child then
                table.insert(indices,i)
            end
        end
        for i = #indices,1,-1 do
            table.remove(self.children,indices[i])
        end
    end
    function instance:onMessage(msg)
        return false
    end
    function instance:handleMessage(msg)
        if self:onMessage(msg) then
            return true
        else
            if self:hasParent() then
                self:getParent():handleMessage(msg)
            end
        end
    end
    function instance:broadcastMessage(msg)
        self:onMessage(msg)
        for i,v in ipairs(self.children) do
            v:broadcastMessage(msg)
        end
    end
    function instance:handleBroadcast(msg,reverse)
        reverse = reverse or false
        if reverse then
            for i in #self.children,1,-1 do
                if self.children[i]:handleBroadcast(msg,reverse) then
                    return true
                end
            end
            return self:onMessage(msg)
        else
            if self:onMessage(msg) then
                return true
            else
                for i in 1, #self.children do
                    if self.children[i]:handleBroadcast(msg,reverse) then
                        return true
                    end
                end
                return false
            end
        end
    end
    instance:setParent(parent)
    return instance
end

return module