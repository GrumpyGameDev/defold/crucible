local states = {}

states.TITLE = function() return "title" end
states.INSTRUCTIONS = function() return "instructions" end
states.AT_SEA = function() return "atSea" end
states.SET_HEADING = function() return "setHeading" end

return states