local constants = require("game.constants")

local avatar_x = 0
local avatar_y = 0
local avatar_degrees = 0
local avatar_minutes = 0
local avatar_seconds = 0
local avatar_speed = 1
local avatar = {}

avatar.setX = function (x)
	avatar_x = x
end

avatar.setY = function(y)
	avatar_y = y
end

avatar.setDegrees = function(degrees)
	avatar_degrees = degrees
end

avatar.setMinutes = function(minutes)
	avatar_minutes = minutes
end

avatar.setSeconds = function(seconds)
	avatar_seconds = seconds
end

avatar.setSpeed = function(speed)
	avatar_speed = speed
end

avatar.getX = function()
	return avatar_x
end

avatar.getY = function()
	return avatar_y
end

avatar.getDegrees = function()
	return avatar_degrees
end

avatar.getMinutes = function()
	return avatar_minutes
end

avatar.getSeconds = function()
	return avatar_seconds
end

avatar.getSpeed = function()
	return avatar_speed
end

avatar.reset = function()
	avatar_x = math.random() * constants.WORLD_WIDTH() / 2 + constants.WORLD_WIDTH()/4
	avatar_y = math.random() * constants.WORLD_HEIGHT() / 2 + constants.WORLD_HEIGHT()/4
	avatar_degrees = math.random(0,359)
	avatar_minutes = math.random(0,59)
	avatar_seconds = math.random(0,59)
	avatar_speed = 1
end

local getRadians=function(d,m,s)
	return ((((s/60)+m)/60)+d)*math.pi/180
end

avatar.move = function()
	avatar_x = avatar_x + math.cos(getRadians(avatar_degrees, avatar_minutes, avatar_seconds))
	avatar_y = avatar_y + math.sin(getRadians(avatar_degrees, avatar_minutes, avatar_seconds))
end

return avatar