local constants = require("game.constants")
local utility = require("game.utility")

local consonants = "HKLMP"
local vowels = "AEIOU"

local data = {}

local checkName=function(name)
	for i, v in ipairs(data) do
		if v.name==name then
			return false
		end
	end
	return true
end

local checkLocation = function(x,y)
	for i, v in ipairs(data) do
		if utility.distance(v.x,v.y,x,y)<constants.MINIMUM_ISLAND_DISTANCE() then
			return false
		end
	end
	return true
end

local generateName = function()
	local nameLength = math.random(1,4)+math.random(1,4)+math.random(1,4)
	local name = ""
	local vowel = math.random(1,2)==1
	while string.len(name) < nameLength do
		local letter = ""
		if vowel then
			local index = math.random(1,string.len(vowels))
			letter = string.sub(vowels, index, index)
		else
			local index = math.random(1,string.len(consonants))
			letter = string.sub(consonants, index, index)
		end
		name = name .. letter
		vowel = not vowel
	end
	return name
end

local islands = {}

islands.reset = function()
	data = {}
	local failCount = 0
	while failCount<500 do
		local x = math.random() * constants.WORLD_WIDTH()
		local y = math.random() * constants.WORLD_HEIGHT()
		if checkLocation(x,y) then
			local island = {}
			island.name = ""
			island.x = x
			island.y = y
			table.insert(data,island)
			failCount=0
		else
			failCount=failCount+1
		end
	end
	for i,v in ipairs(data) do
		local name = generateName()
		if checkName(name) then
			v.name = name
		end
	end
end

islands.data = function()
	return data
end

return islands