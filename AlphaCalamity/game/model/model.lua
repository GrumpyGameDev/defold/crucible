local avatar = require("game.model.avatar")
local islands = require("game.model.islands")
local gameStates = require("game.states")

local model = {}

model.reset = function()
	avatar.reset()
	islands.reset()
end

model.gleanState = function()
	return gameStates.AT_SEA()
end

model.getAvatar = function()
	return avatar
end

model.getIslands = function()
	return islands
end

model.moveAvatar = function()
	if model.gleanState() == gameStates.AT_SEA() then
		avatar.move()
	end
end

return model