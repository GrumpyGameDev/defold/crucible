local commands = {}

commands.LEFT = function() return "left" end
commands.UP = function() return "up" end
commands.RIGHT = function() return "right" end
commands.DOWN = function() return "down" end
commands.GREEN = function() return "green" end
commands.RED = function() return "red" end

return commands