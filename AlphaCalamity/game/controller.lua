local controller = {}
local current = require("game.states.current")
local gameStates = require("game.states")

local states = {}
states[gameStates.TITLE()] = require("game.states.title")
states[gameStates.INSTRUCTIONS()] = require("game.states.instructions")
states[gameStates.AT_SEA()] = require("game.states.atSea")
states[gameStates.SET_HEADING()] = require("game.states.setHeading")

local updateView = function ()
	states[current.getState()].updateView()
end

local handleCommand = function(command)
	states[current.getState()].handleCommand(command)
end

controller.handleCommand = handleCommand
controller.updateView = updateView

return controller