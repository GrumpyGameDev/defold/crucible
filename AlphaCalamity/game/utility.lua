local utility = {}

utility.displayNumber = function(number)
	return math.floor(number*100)/100
end

utility.distance = function(x1,y1,x2,y2)
	return math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))
end

utility.fif = function(condition,whenTrue,whenFalse)
	if condition then
		return whenTrue
	else
		return whenFalse
	end
end


return utility