local constants = {}

constants.WORLD_WIDTH = function() return 100 end
constants.WORLD_HEIGHT = function() return 100 end
constants.MINIMUM_ISLAND_DISTANCE = function() return 10 end
constants.AVATAR_VIEW_DISTANCDE = function() return 10 end
constants.AVATAR_DOCK_DISTANCE = function() return 1 end

return constants