local instructions = {}
local screen = require("game.screen")
local current = require("game.states.current")
local model = require("game.model.model")
local commands = require("game.commands")
local gameStates = require("game.states")

local updateView = function ()
	screen.clear(96)
	screen.writeText(1,16,"CONTROLS:")
	screen.writeText(1,15,"ARROW KEYS: MOVE/SELECT")
	screen.writeText(1,14,"Z: ACCEPT")
	screen.writeText(1,13,"X: CANCEL")
	screen.writeText(11,1,"<Z> TO START")
end

local handleCommand = function(command)
	if command==commands.GREEN() then
		model.reset()
		current.setState(model.gleanState())
	end
end

instructions.handleCommand = handleCommand
instructions.updateView = updateView

return instructions