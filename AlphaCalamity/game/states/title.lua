local title = {}
local screen = require("game.screen")
local current = require("game.states.current")
local commands = require("game.commands")

local updateView = function ()
	screen.clear(191)
	screen.writeText(9,9,"ALPHA CALAMITY!!")
	screen.writeText(12,16,"A GAME  BY")
	screen.writeText(9,15,"THEGRUMPYGAMEDEV")
	screen.writeText(11,1,"<Z> TO START")
end

local handleCommand = function(command)
	if command==commands.GREEN() then
		current.setState("instructions")
	end
end

title.handleCommand = handleCommand
title.updateView = updateView

return title