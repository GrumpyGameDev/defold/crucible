local screen = require("game.screen")
local current = require("game.states.current")
local model = require("game.model.model")
local utility = require("game.utility")
local commands = require("game.commands")
local constants = require("game.constants")

local MOVE_AVATAR_COMMAND = "MOVE"
local SET_HEADING_COMMAND = "SET HEADING"
local SET_SPEED_COMMAND = "SET SPEED"
local DOCK_COMMAND = "DOCK"

local menuItem = 1
local menuItems = {}

local canDock = function()
	local islands = model.getIslands().data()
	for i, v in ipairs(islands) do
		if utility.distance(model.getAvatar().getX(),model.getAvatar().getY(),v.x,v.y) <= constants.AVATAR_DOCK_DISTANCE() then
			return true
		end
	end
	return false
end

local determineMenuItems = function()
	menuItems = {}
	table.insert(menuItems, MOVE_AVATAR_COMMAND)
	table.insert(menuItems, SET_HEADING_COMMAND)
	table.insert(menuItems, SET_SPEED_COMMAND)
	if canDock() then
		table.insert(menuItems, DOCK_COMMAND)
	end

	if menuItem>#menuItems then
		menuItem = 1
	end
end

local updateNearbyIslands = function(line)
	screen.writeText(1,line,"NEARBY ISLANDS:")
	line = line - 1
	local islands = model.getIslands().data()
	local nearby = {}
	for i, v in ipairs(islands) do
		if utility.distance(model.getAvatar().getX(),model.getAvatar().getY(),v.x,v.y) <= constants.AVATAR_VIEW_DISTANCDE() then
			table.insert(nearby, v)
		end
	end
end

local updateView = function ()
	local avatar = model.getAvatar()
	screen.clear(96)
	screen.writeText(1,16,"AT SEA")
	screen.writeText(1,15,"LOCATION: ("..utility.displayNumber(avatar.getX())..","..utility.displayNumber(avatar.getY())..")")
	screen.writeText(1,14,"HEADING: "..avatar.getDegrees().." "..avatar.getMinutes().."'"..avatar.getSeconds().."\"")
	screen.writeText(1,13,"SPEED: "..utility.displayNumber(avatar.getSpeed()))
	determineMenuItems()
	for index = 1, #menuItems do
		screen.writeText(2,13-index,menuItems[index])
	end
	updateNearbyIslands(13-#menuItems-1)
	screen.writeText(1,13-menuItem,">")
end

local doMenuItem = function()
	local command = menuItems[menuItem]
	if command == MOVE_AVATAR_COMMAND then
		model.moveAvatar()
	elseif command == SET_HEADING_COMMAND then
		current.setState("setHeading")
	elseif command == SET_SPEED_COMMAND then
	end
end

local handleCommand = function(command)
	if command==commands.UP() then
		menuItem = menuItem - 1
		if menuItem<1 then 
			menuItem=#menuItems
		end
	elseif command==commands.DOWN() then
		menuItem = menuItem + 1
		if menuItem>#menuItems then 
			menuItem=1
		end
	elseif command==commands.GREEN() then
		doMenuItem()
	end
end

local atSea = {}

atSea.handleCommand = handleCommand
atSea.updateView = updateView

return atSea