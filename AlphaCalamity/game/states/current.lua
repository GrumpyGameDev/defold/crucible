local gameStates = require("game.states")

local currentState = gameStates.TITLE()

local setState = function(newState)
	currentState = newState
end

local getState = function()
	return currentState
end

local current = {}

current.setState = setState
current.getState = getState

return current