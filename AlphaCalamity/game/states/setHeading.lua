local screen = require("game.screen")
local current = require("game.states.current")
local model = require("game.model.model")
local commands = require("game.commands")
local gameStates = require("game.states")

local NINETY_DEGREES = "90 DEGREES"
local FORTYFIVE_DEGREES = "45 DEGREES"
local THIRTY_DEGREES = "30 DEGREES"
local FIFTEEN_DEGREES = "15 DEGREES"
local ONE_DEGREE = "1 DEGREES"
local TEN_MINUTES = "10'"
local ONE_MINUTE = "1'"
local TEN_SECONDS = "10\""
local ONE_SECOND = "1\""
local DONE = "DONE"
local degreeTable = {}
degreeTable[NINETY_DEGREES] = 90
degreeTable[FORTYFIVE_DEGREES] = 45
degreeTable[THIRTY_DEGREES] = 30
degreeTable[FIFTEEN_DEGREES] = 15
degreeTable[ONE_DEGREE]= 1
degreeTable[TEN_MINUTES] = 0
degreeTable[ONE_MINUTE] = 0
degreeTable[TEN_SECONDS] = 0
degreeTable[ONE_SECOND] = 0
degreeTable[DONE] = 0
local minuteTable = {}
minuteTable[NINETY_DEGREES] = 0
minuteTable[FORTYFIVE_DEGREES] = 0
minuteTable[THIRTY_DEGREES] = 0
minuteTable[FIFTEEN_DEGREES] = 0
minuteTable[ONE_DEGREE]= 0
minuteTable[TEN_MINUTES] = 10
minuteTable[ONE_MINUTE] = 1
minuteTable[TEN_SECONDS] = 0
minuteTable[ONE_SECOND] = 0
minuteTable[DONE] = 0
local secondTable = {}
secondTable[NINETY_DEGREES] = 0
secondTable[FORTYFIVE_DEGREES] = 0
secondTable[THIRTY_DEGREES] = 0
secondTable[FIFTEEN_DEGREES] = 0
secondTable[ONE_DEGREE]= 0
secondTable[TEN_MINUTES] = 0
secondTable[ONE_MINUTE] = 0
secondTable[TEN_SECONDS] = 10
secondTable[ONE_SECOND] = 1
secondTable[DONE] = 0
local menuItems = {}
local menuItem=1

local determineMenuItems = function()
	menuItems = {}
	table.insert(menuItems, DONE)
	table.insert(menuItems, NINETY_DEGREES)
	table.insert(menuItems, FORTYFIVE_DEGREES)
	table.insert(menuItems, THIRTY_DEGREES)
	table.insert(menuItems, FIFTEEN_DEGREES)
	table.insert(menuItems, ONE_DEGREE)
	table.insert(menuItems, TEN_MINUTES)
	table.insert(menuItems, ONE_MINUTE)
	table.insert(menuItems, TEN_SECONDS)
	table.insert(menuItems, ONE_SECOND)
end

local updateView = function ()
	local avatar = model.getAvatar()
	screen.clear(96)
	screen.writeText(1,16,"SET HEADING")
	screen.writeText(1,15,"CURRENT: "..avatar.getDegrees().." "..avatar.getMinutes().."'"..avatar.getSeconds().."\"")
	determineMenuItems()
	for index = 1, #menuItems do
		screen.writeText(2,15-index,menuItems[index])
	end
	screen.writeText(1,15-menuItem,">")
	screen.writeText(1,1,"LEFT ARROW: -; RIGHT ARROW: +")
end

local doMenuItem = function()
	local command = menuItems[menuItem]
	if command == DONE then
		current.setState(gameStates.AT_SEA())
	end
end

local minusMenuItem = function()
	local seconds = model.getAvatar().getSeconds()
	local minutes = model.getAvatar().getMinutes()
	local degrees = model.getAvatar().getDegrees()
	local command = menuItems[menuItem]
	seconds = seconds - secondTable[command]
	if seconds<0 then
		seconds = seconds + 60
		minutes = minutes - 1
	end
	minutes = minutes - minuteTable[command]
	if minutes<0 then
		minutes = minutes + 60
		degrees = degrees - 1
	end
	degrees = degrees - degreeTable[command]
	if degrees<0 then
		degrees = degrees + 360
	end
	model.getAvatar().setSeconds(seconds)
	model.getAvatar().setMinutes(minutes)
	model.getAvatar().setDegrees(degrees)
end

local plusMenuItem = function()
	local seconds = model.getAvatar().getSeconds()
	local minutes = model.getAvatar().getMinutes()
	local degrees = model.getAvatar().getDegrees()
	local command = menuItems[menuItem]
	seconds = seconds + secondTable[command]
	if seconds>59 then
		seconds = seconds - 60
		minutes = minutes + 1
	end
	minutes = minutes + minuteTable[command]
	if minutes>59 then
		minutes = minutes - 60
		degrees = degrees + 1
	end
	degrees = degrees + degreeTable[command]
	if degrees>359 then
		degrees = degrees - 360
	end
	model.getAvatar().setSeconds(seconds)
	model.getAvatar().setMinutes(minutes)
	model.getAvatar().setDegrees(degrees)
end

local handleCommand = function(command)
	if command==commands.UP() then
		menuItem = menuItem - 1
		if menuItem<1 then 
			menuItem=#menuItems
		end
	elseif command==commands.DOWN() then
		menuItem = menuItem + 1
		if menuItem>#menuItems then 
			menuItem=1
		end
	elseif command==commands.LEFT() then
		minusMenuItem()
	elseif command==commands.RIGHT() then
		plusMenuItem()
	elseif command==commands.GREEN() then
		doMenuItem()
	elseif command==commands.RED() then
		current.setState(gameStates.AT_SEA())
	end
end

local handler = {}

handler.handleCommand = handleCommand
handler.updateView = updateView

return handler