local screen = {}
local url = "#screen"
local layer = "screen"
local CELL_COLUMNS = 32
local CELL_ROWS = 16

local putTile = function (x,y,value)
	if x>=1 and y>=1 and x<=CELL_COLUMNS and y<=CELL_ROWS then
		tilemap.set_tile(url, layer, x, y, value+1)
	end
end

local clear = function (value)
	for row = 1,CELL_ROWS do
		for column = 1,CELL_COLUMNS do
			putTile(column,row,value)
		end
	end
end

local writeText = function (x,y,text)
	for index=1,string.len(text) do
		local c = string.byte(text,index)
		if c>=32 and c<64 then
			c = c + 64
		elseif c>=96 and c<128 then
			c = c - 64
		end
		putTile(x,y,c)
		x = x + 1
	end
end

screen.clear = clear
screen.writeText = writeText

return screen